# Dockerfile exemple
## Ce projet est issus de mon article sur Dev.to
https://dev.to/simerca

Bonjour les apprentis **dockeriste** !
Si vous êtes la c'est que comme moi à un certain moment, vous avez décider d'en savoir plus sur ce fichier Dockerfile qui parait simple à la lecture mais pourtant... pas tant que cela.

Commençons avec la question bateau.

### Qu'est-ce que le Dockerfile ?
C'est un petit bout de fichier de configuration qui vient donner à docker ce dont vous avez envie.

Par exemple;

Vous avez une super application en Node JS, vous souhaitez donc la mettre en production sur votre serveur qui accueille des containers dockers. Vous avez besoin de plusieurs chose :

- Lancer une archi Linux,
- Installer les dépendance npm de votre projet.
- Lancer le build et exposer l'execution du script sur le port 80

Dans ce cas le Dockerfile s'exécutera comme cela :
```
FROM debian:9
RUN apt-get update -yq \
&& apt-get install curl gnupg -yq \
&& curl -sL https://deb.nodesource.com/setup_10.x | bash \
&& apt-get install nodejs -yq \
&& apt-get clean -y

WORKDIR /app
COPY app/package*.json ./
RUN npm install
COPY app .

RUN npm run build

EXPOSE 80

CMD npm run start
```

Lisons un peu ce bout de code

- On utilise l'image de Debian 9
- On execute la mise à jour des paquets et l'installation de node
- On définie le repertoire de travail dans son dossier app
- On lance l'installation des dépendances
- On build
- On expose sur le port 80
- Et on lance notre application

ensuite on ajoute un fichier **.dockerignore**, il fonctionne comme un **.gitignore**, c'est à dire que l'on va ignorer certain fichier / dossier de notre projet
exemple :

```
.git
app/node_modules
```
ensuite,
je vous conseil une architecture de vos projets comme ceci

```
root:
  Dockerfile
  app/
    node_modules
    src
    packages.json
    etc
```
*merci Xavier*

allez-y tester avec la commande suivante :
```
docker build -t nomdevotreimage .
```

youhouuu ! vous avez build votre premier Dockerfile félicitations !
